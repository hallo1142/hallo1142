# About me

Hey 👋,

I'm Kevin and currently 20 years old. I'm currently a student at FAU Erlangen-Nuremberg studying computer science. Next to university I'm working at Hetzner, a hosting provider, in technical support. In my freetime I mainly do system administrative jobs for a project with a friend and I'm also trying to get more experience in clouds.

Those are the projects I'm currently working on:

 - [Zyonic](https://zyonicsoftware.com)
 - [Uroria](https://uroria.com)

My self-hosted GitLab profile can be found [here](https://gitlab.zyonicsoftware.com/hallo1142).

Recently I made the achievement to become a "AWS Certified Cloud Practitioner":

[![Image Badge](https://i.ibb.co/HB013RN/aws-certified-cloud-practitioner.png)](https://www.credly.com/badges/004619b2-0739-4d5d-92bc-43e90606f166/public_url)


I plan to get more badges in the future, also in different areas but for now this badge is all I have.